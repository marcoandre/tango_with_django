# Notas de Aula #

Anotações de aula, dicas, links, etc.

## Links importantes ##

* [How to Tango with Django](http://www.tangowithdjango.com/)
* [Django](https://www.djangoproject.com/)

## TODO: ##

* Colocar views em arquivos separados
* Colocar models em arquivos separados
* Mudar o return das views para o padrão return '''render_to_response('index.html', context_dict, RequestContext(request))'''

## Projeto e especificação inicial da aplicação ##

Vamos desenvolver uma aplicação chamada Rango. À medida desenvolvemos essa aplicação, cobriremos os componentes chave que necessitam ser desenvolvidos na construção de qualquer aplicação web.

### Resumo do projeto ###

Seu cliente gostaria de criar um website chamado Rango, que  permite que os usuários naveguem através das categorias definidas pelo usuário para acessar várias páginas web. Em espanhol, a palavra rango é usada para definir um "uma liga classificada por qualidade" ou "uma posição numa hierarquia social".

Para a página principal do site, eles gostariam que os visitantes pudessem ver:

* as 5 páginas mais visitadas;
* as 5 categorias melhor classificadas; e
* uma forma de vistantes navegarem ou pesquisarem através das categorias.

Quando um usuário visualizar uma página de categoria, eles gostariam de mostrar:

* o nome da categoria, o número de visitantes, o números de "likes";
* junto de uma lista de páginas associadas àquela categoria (mostrando o título da página e ligado a sua url);

Para uma categoria em particular, o cliente que o nome da categoria fosse gravado, o número de vezes que a página da categoria foi visitada, e quantos usuários clicaram no botão "like" (isto é, a página é classificada e sobe na hierarquia da votação).

Cada categoria deveria ser acessada através de uma URL legível, por exemplo, /rango/books-about-django/.

Somente usuários registrados devem ser capazes de pesquisar e adicionar categorias. Visitantes devem poder registrar uma conta.

Numa relance inicial, a aplicação a ser desenvolvida parece bem simples. Na sua essência, é apenas uma lista de categorias que conectam a páginas, certo? Entretanto, há um número de complexidades e desafios que precisam ser endereçados. 

### Instalação de virtualenv e pip 

* `sudo apt-get install python-pip`
* `sudo pip install virtualenv`

### Verificação das versões do python e django:

* `python --version`
* `python -c "import django; print(django.get_version())`
