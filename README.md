# Notas de Aula #

Anotações de aula, dicas, links, etc.

## Links importantes ##

* [How to Tango with Django](http://www.tangowithdjango.com/)
* [Django](https://www.djangoproject.com/)

## TODO: ##

* Colocar views em arquivos separados
* Colocar models em arquivos separados
* Mudar o return das views para o padrão return '''render_to_response('index.html', context_dict, RequestContext(request))'''

## Projeto e especificação inicial da aplicação ##

Vamos desenvolver uma aplicação chamada Rango. À medida desenvolvemos essa aplicação, cobriremos os componentes chave que necessitam ser desenvolvidos na construção de qualquer aplicação web.

### Resumo do projeto ###

Seu cliente gostaria de criar um website chamado Rango, que  permite que os usuários naveguem através das categorias definidas pelo usuário para acessar várias páginas web. Em espanhol, a palavra rango é usada para definir um "uma liga classificada por qualidade" ou "uma posição numa hierarquia social".

Para a página principal do site, eles gostariam que os visitantes pudessem ver:

* as 5 páginas mais visitadas;
* as 5 categorias melhor classificadas; e
* uma forma de vistantes navegarem ou pesquisarem através das categorias.

Quando um usuário visualizar uma página de categoria, eles gostariam de mostrar:

* o nome da categoria, o número de visitantes, o números de "likes";
* junto de uma lista de páginas associadas àquela categoria (mostrando o título da página e ligado a sua url);

Para uma categoria em particular, o cliente que o nome da categoria fosse gravado, o número de vezes que a página da categoria foi visitada, e quantos usuários clicaram no botão "like" (isto é, a página é classificada e sobe na hierarquia da votação).

Cada categoria deveria ser acessada através de uma URL legível, por exemplo, /rango/books-about-django/.

Somente usuários registrados devem ser capazes de pesquisar e adicionar categorias. Visitantes devem poder registrar uma conta.

Numa relance inicial, a aplicação a ser desenvolvida parece bem simples. Na sua essência, é apenas uma lista de categorias que conectam a páginas, certo? Entretanto, há um número de complexidades e desafios que precisam ser endereçados. 

## Configurações do ambiente:

### Instalação de git, pip e virtualenv

* `sudo apt-get install git python-pip`
* `sudo pip install virtualenv`

### Verificação das versões do python e django:

* `python --version`
* `python -c "import django; print(django.get_version())`

### Configuração inicial do git na máquina:
* `git config --global user.name "John Doe"`
* `git config --global user.email johndoe@example.com`
* `git config --global core.editor nano`
* `git config --global credential.helper 'cache --timeout=3600'`

### Instalação do Pycharm:
* copiar o arquivo pycharm-3.4.1.tgz para a pasta /opt/:
`sudo cp /media/bla/pycharm-3.4.1.tgz /opt`
* descompactar o arquivo:
`sudo tar -xzvf pycharm-3.4.1.tgz`
* copiar o arquivo de ícone para a pasta correta:
`sudo cp /opt/pycharm-3.4.1/pycharm.desktop /usr/share/applications/`
* criar um link simbólico para a pasta do pycharm:
`sudo ln -s /opt/pycharm-3.4.1 /opt/pycharm`
* Licença - usuário:
`Instituto Federal Catarinense`
* Licença chave:
`488075-30052014
00000"zcyIZ!vklKZe4mnewqb7Pd3S
ngn90tKswdqvlANfT0"QpxcAMmOrNG
1cF!y"lzagjJ9Vfh3ZuBLeRUzn5a0q`

## Exercícios cap. 13:

### Integrar a navegação nas categorias, isto é:
* fornecer categorias em todas as páginas;
### Fornecer serviços para usuários registrador,isto é:
* permitir que usuários vejam seu perfil;
* permitir que usuários editem seu perfil;
* permitir que usuários veja a lista de usuários e seus perfis;
### Monitorar os cliques através das Categorias e Páginas, isto é:
* contar o número de vezes que uma categoria foi vista;
* contar o número de vezes que uma página foi vista pelo Rango;
