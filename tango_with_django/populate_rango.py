#!/usr/bin/env python
# coding: utf-8


import os

def popular():
    python_cat = adiciona_categoria(nome='Python', visitas=128, curtidas=64)

    adiciona_pagina(categoria=python_cat,
        titulo="Official Python Tutorial",
        url="http://docs.python.org/2/tutorial/",
        visitas=10)

    adiciona_pagina(categoria=python_cat,
        titulo="How to Think like a Computer Scientist",
        url="http://www.greenteapress.com/thinkpython/",
        visitas=5)

    adiciona_pagina(categoria=python_cat,
        titulo="Learn Python in 10 Minutes",
        url="http://www.korokithakis.net/tutorials/python/")

    django_cat = adiciona_categoria("Django", 64, 32)

    adiciona_pagina(categoria=django_cat,
        titulo="Official Django Tutorial",
        url="https://docs.djangoproject.com/en/1.5/intro/tutorial01/",
        visitas=7)

    adiciona_pagina(categoria=django_cat,
        titulo="Django Rocks",
        url="http://www.djangorocks.com/")

    adiciona_pagina(categoria=django_cat,
        titulo="How to Tango with Django",
        url="http://www.tangowithdjango.com/",
        visitas=20)

    frame_cat = adiciona_categoria("Other Frameworks", curtidas=16, visitas=32)

    adiciona_pagina(categoria=frame_cat,
        titulo="Bottle",
        url="http://bottlepy.org/docs/dev/")

    adiciona_pagina(categoria=frame_cat,
        titulo="Flask",
        url="http://flask.pocoo.org",
        visitas=1)

    # Print out what we have added to the user.
    for c in Categoria.objects.all():
        for p in Pagina.objects.filter(categoria=c):
            print "- {0} - {1}".format(str(c), str(p))

def adiciona_pagina(categoria, titulo, url, visitas=0):
    p = Pagina.objects.get_or_create(categoria=categoria, titulo=titulo, url=url, visitas=visitas)[0]
    return p

def adiciona_categoria(nome, visitas=0, curtidas=0):
    c = Categoria.objects.get_or_create(nome=nome, visitas=visitas, curtidas=curtidas)[0]
    return c

# Start execution here!
if __name__ == '__main__':
    print "Iniciando script Rango para popular base de dados..."
    os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'tango_with_django.settings')
    from rango.models import Categoria, Pagina
    popular()