from django.db import models
from django.contrib.auth.models import User

class Categoria(models.Model):
    nome = models.CharField(max_length=128, unique=True)
    visitas = models.IntegerField(default=0)
    curtidas = models.IntegerField(default=0)

    def __unicode__(self):
        return self.nome

class Pagina(models.Model):
    categoria = models.ForeignKey(Categoria)
    titulo = models.CharField(max_length=128)
    url = models.URLField()
    visitas = models.IntegerField(default=0)

    def __unicode__(self):
        return self.titulo

class UserProfile(models.Model):
    user = models.OneToOneField(User)

    website = models.URLField(blank=True)
    picture = models.ImageField(upload_to='profile_images', blank=True)

    def __unicode__(self):
        return self.user.username
