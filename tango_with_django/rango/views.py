#encoding: utf-8
from django.template import RequestContext
from django.shortcuts import render_to_response, redirect
from django.contrib.auth import authenticate, login
from django.http import HttpResponseRedirect, HttpResponse
from django.contrib.auth.decorators import login_required
from django.contrib.auth import logout
from django.contrib.auth.models import User

from rango.models import Categoria, Pagina, UserProfile
from rango.forms import CategoriaForm, PaginaForm
from rango.forms import UserForm, UserProfileForm

from datetime import datetime

def encode_url(url):
    return url.replace(' ', '_')

def decode_url(url):
    return url.replace('_', ' ')

def get_categoria_lista():
    categoria_lista = Categoria.objects.order_by('-curtidas')
    for categoria in categoria_lista:
        categoria.url = encode_url(categoria.nome)

    return categoria_lista

def index(request):
    context = RequestContext(request)
    context_dict = {}
    categoria_lista = get_categoria_lista()
    context_dict['categoria_lista'] = categoria_lista

    categoria_lista_top = Categoria.objects.order_by('-curtidas')[:5]
    context_dict['categoria_lista_top'] = categoria_lista_top

    for categoria in categoria_lista_top:
        categoria.url = encode_url(categoria.nome)

    paginas_mais_visitadas_lista = Pagina.objects.order_by('-visitas')[:5]
    context_dict['paginas'] = paginas_mais_visitadas_lista

    if request.session.get('last_visit'):
        last_visit_time = request.session.get('last_visit')
        visits = request.session.get('visits', 0)

        if (datetime.now() - datetime.strptime(last_visit_time[:-7], "%Y-%m-%d %H:%M:%S")).seconds > 5:
            request.session['visits'] = visits + 1
            request.session['last_visit'] = str(datetime.now())
    else:
        request.session['last_visit'] = str(datetime.now())

    return render_to_response('rango/index.html', context_dict, context)

def sobre(request):
    context = RequestContext(request)
    context_dict = {}
    categoria_lista = get_categoria_lista()
    context_dict['categoria_lista'] = categoria_lista

    boldmessage = "Essa é a página sobre o Rango!"
    context_dict['boldmessage'] = boldmessage
    if request.session.get('visits'):
        count = request.session.get('visits')
    else:
        count = 0
    context_dict['contador_visitas'] = count

    return render_to_response('rango/sobre.html', context_dict, context)

def categoria(request, categoria_nome_url):
    context = RequestContext(request)
    context_dict = {}
    categoria_lista = get_categoria_lista()
    context_dict['categoria_lista'] = categoria_lista

    categoria_nome = decode_url(categoria_nome_url)

    context_dict['categoria_nome'] = categoria_nome
    context_dict['categoria_nome_url'] = categoria_nome_url

    try:
        categoria = Categoria.objects.get(nome=categoria_nome)
        paginas = Pagina.objects.filter(categoria=categoria).order_by('-visitas')
        context_dict['paginas'] = paginas
        context_dict['categoria'] = categoria
    except Categoria.DoesNotExist:
        pass
        #return index(request) # Se a categoria não existe, manda pra Home

    return render_to_response('rango/categoria.html', context_dict, context)

@login_required
def adiciona_categoria(request):
    context = RequestContext(request)
    context_dict = {}
    categoria_lista = get_categoria_lista()
    context_dict['categoria_lista'] = categoria_lista

    print request.POST
    if request.method == "POST":
        form = CategoriaForm(request.POST)
        if form.is_valid():
            form.save(commit=True)
            return index(request)
        else:
            print form.errors
    else:
        form = CategoriaForm()

    context_dict['form'] = form
    return render_to_response('rango/adiciona_categoria.html', context_dict, context)

@login_required
def adiciona_pagina(request, categoria_nome_url):
    context = RequestContext(request)
    context_dict = {}
    categoria_lista = get_categoria_lista()
    context_dict['categoria_lista'] = categoria_lista

    categoria_nome = decode_url(categoria_nome_url)

    if request.method != "POST":
        form = PaginaForm()
    else:
        form = PaginaForm(request.POST)
        if not form.is_valid():
            print form.errors
        else:
            pagina = form.save(commit=False)
            try:
                cat = Categoria.objects.get(nome=categoria_nome)
                pagina.categoria = cat
            except Categoria.DoesNotExist:
                return adiciona_categoria(request)

            pagina.visitas = 0
            pagina.save()
            return categoria(request, categoria_nome_url)

    context_dict['categoria_nome'] = categoria_nome
    context_dict['categoria_nome_url'] = categoria_nome_url
    context_dict['form'] = form

    return render_to_response('rango/adiciona_pagina.html', context_dict, context)

def registro(request):
    context = RequestContext(request)
    context_dict = {}
    categoria_lista = get_categoria_lista()
    context_dict['categoria_lista'] = categoria_lista

    registrado = False

    if request.method != "POST":
        user_form = UserForm
        user_profile_form = UserProfileForm
    else:
        user_form = UserForm(data=request.POST)
        user_profile_form = UserProfileForm(data=request.POST)
        if not user_form.is_valid() or not user_profile_form.is_valid():
            print user_form.errors, user_profile_form.errors
        else:
            user = user_form.save()
            user.set_password(user.password)
            user.save()

            user_profile = user_profile_form.save(commit=False)
            user_profile.user = user

            if 'picture'in request.FILES:
                user_profile.picture = request.FILES['picture']

            user_profile.save()
            registrado = True


    context_dict['user_form'] = user_form
    context_dict['user_profile_form'] = user_profile_form
    context_dict['registrado'] = registrado

    return render_to_response('rango/registro.html', context_dict, context)

def user_login(request):
    context = RequestContext(request)
    context_dict = {}
    categoria_lista = get_categoria_lista()
    context_dict['categoria_lista'] = categoria_lista

    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']

        user = authenticate(username=username, password=password)

        if user:
            if user.is_active:
                login(request, user)
                return HttpResponseRedirect('/rango/')
            else:
                context_dict['conta_desabilitada'] = True
                return render_to_response('rango/login.html', context_dict, context)
        else:
            print "Detalhes de login inválidos: {0}, {1}".format(username, password)
            context_dict['detalhes_ruins'] = True
            return render_to_response('rango/login.html', context_dict, context)
    else:
        return render_to_response('rango/login.html', context_dict, context)

@login_required
def restrito(request):
    context = RequestContext(request)
    context_dict = {}
    categoria_lista = get_categoria_lista()
    context_dict['categoria_lista'] = categoria_lista

    return render_to_response('rango/restrito.html', context_dict, context)

@login_required
def user_logout(request):
    logout(request)

    return HttpResponseRedirect('/rango/')

@login_required
def profile(request):
    context = RequestContext(request)
    context_dict = {}
    categoria_lista = get_categoria_lista()
    context_dict['categoria_lista'] = categoria_lista
    u = User.objects.get(username=request.user)

    try:
        up = UserProfile.objects.get(user=u)
    except:
        up = None

    context_dict['user'] = u
    context_dict['userprofile'] = up

    return render_to_response('rango/profile.html', context_dict, context)
#    return HttpResponse(u,up)

def track_url(request):
    context = RequestContext(request)
    page_id = None
    url = '/rango/'
    if request.method == 'GET':
        if 'page_id' in request.GET:
            page_id = request.GET['page_id']
            try:
                pagina = Pagina.objects.get(id=page_id)
                pagina.visitas += 1
                pagina.save()
                url = pagina.url
            except:
                pass
    return redirect(url)