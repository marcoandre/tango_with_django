#coding: utf-8
from django import forms
from django.contrib.auth.models import User
from rango.models import Pagina, Categoria, UserProfile

class CategoriaForm(forms.ModelForm):
    nome = forms.CharField(max_length=128, help_text="Informe o nome da categoria")
    visitas = forms.IntegerField(widget=forms.HiddenInput(), initial=0)
    curtidas = forms.IntegerField(widget=forms.HiddenInput(), initial=0)

    class Meta:
        model = Categoria

class PaginaForm(forms.ModelForm):
    titulo = forms.CharField(max_length=128, help_text="Informe o titulo da página")
    url = forms.URLField(max_length=200, help_text="Informe a URL da página")
    visitas = forms.IntegerField(widget=forms.HiddenInput(), initial=0)

    def clean(self):
        cleaned_data = self.cleaned_data
        url = cleaned_data.get('url')

        if url and not url.startswith('http://'):
            url = 'http://' + url
            cleaned_data['url'] = url

        return cleaned_data

    class Meta:
        model = Pagina
        fields = ('titulo', 'url', 'visitas')

class UserForm(forms.ModelForm):
    username = forms.CharField(help_text="Informe o nome do usuário.")
    email = forms.CharField(help_text="Informe seu email.")
    password = forms.CharField(widget=forms.PasswordInput(), help_text="Informe sua senha.")

    class Meta:
        model = User
        fields = ['username', 'email', 'password']

class UserProfileForm(forms.ModelForm):
    website = forms.URLField(help_text="Informe seu website.", required=False)
    picture = forms.ImageField(help_text="Selecione uma imagem de perfil para fazer o upload.", required=False)

    class Meta:
        model = UserProfile
        fields = ['website', 'picture']