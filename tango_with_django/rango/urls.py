from django.conf.urls import patterns, url
from rango import views

urlpatterns = patterns('',
        url(r'^$', views.index, name='index'),
        url(r'^sobre/$', views.sobre, name='sobre'),
        url(r'^registro/$', views.registro, name='registro'),
        url(r'^login/$', views.user_login, name='login'),
        url(r'^restrito/', views.restrito, name='restrito'),
        url(r'^logout/$', views.user_logout, name='logout'),
        url(r'^perfil/$', views.profile, name='profile'),
        url(r'^adiciona_categoria/$',
            views.adiciona_categoria, name='adiciona_categoria'),
        url(r'^goto/$', views.track_url, name='track_url'),
        url(r'^categoria/(?P<categoria_nome_url>\w+)/$',
            views.categoria, name='categoria'),
        url(r'^categoria/(?P<categoria_nome_url>\w+)/adiciona_pagina/$',
            views.adiciona_pagina, name='adiciona_pagina'),
)
