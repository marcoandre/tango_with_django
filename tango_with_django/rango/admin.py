from django.contrib import admin
from rango.models import Categoria, Pagina, UserProfile

class PaginaAdmin(admin.ModelAdmin):
    list_display = ('categoria', 'titulo', 'url', 'visitas')
    ordering = ('categoria',)

admin.site.register(Categoria)
admin.site.register(Pagina, PaginaAdmin)
admin.site.register(UserProfile)